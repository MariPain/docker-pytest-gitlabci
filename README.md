### Concept 1 : Dockeriser une Application Python Simple
Étape 1 : Créer l'application Flask
Étape 2 : Créer le fichier requirements.txt
Étape 3 : Créer le Dockerfile
### Concept 2 : Écrire des Tests Unitaires avec Pytest
Étape 1 : Créer un répertoire nommé “tests”
Étape 2 : Créer le fichier de test `test_app.py` 
### Concept 3 : Créer un Pipeline GitLab CI
Étape 1 : Créer le fichier `.gitlab-ci.yml`
### Directives de Soumission
Assurez-vous que la structure du répertoire est conforme aux spécifications 

- app.py
- requirements.txt
- Dockerfile
- tests/ test_app.py
- .gitlab-ci.yml

### Critères d'Évaluation
Correction :

Vérifiez que l'application Flask fonctionne correctement lorsqu'elle est containerisée.
Lancez l'application avec Docker 
Ouvrez un navigateur et allez à http://localhost:5000 pour voir "Bonjour, le monde!".

### Tests :
Exécutez les tests Pytest localement.
Assurez-vous que les tests passent.

### Pipeline CI :
Assurez-vous que le pipeline GitLab CI est correctement configuré et complète toutes les étapes avec succès.
Poussez les changements à la branche main et vérifiez l'exécution du pipeline sur GitLab.

### Code Propre :

Le code est bien organisé et suit les meilleures pratiques.


